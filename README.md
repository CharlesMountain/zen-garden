## Stephen
1. Media queries affecting the layout to enable responsive behavior
2. Font choices
3. Found the pizza image

## Chris
1. Footer, List and Links
2. Colors

## Olia
1. Body: 
	- background image transparancy 
	- background animation
2. Header:
	- "Zen Garden" string is a png image: always on the top left, displayed as a box
	- "The beauty of CSS Design" string: letters have a stroke; first letter has an additional style. 

## Charles
1. Borders
2. Transparency 